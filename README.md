# Rubro

Rubro is a directory and file organization tool for backups.

It's currently under design phase, so there's no code yet. But I need this to solve my backup problem, so there
will be code very soon.

## Want to know more?

[go here](https://gitlab.com/Kores/rubro/-/blob/main/DRAFT.md)